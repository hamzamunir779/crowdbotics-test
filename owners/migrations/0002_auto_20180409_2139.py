# -*- coding: utf-8 -*-
# Generated by Django 1.11.12 on 2018-04-09 21:39
from __future__ import unicode_literals

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('owners', '0001_initial'),
    ]

    operations = [
        migrations.AddField(
            model_name='owner',
            name='created_at',
            field=models.DateTimeField(auto_now_add=True, null=True),
        ),
        migrations.AddField(
            model_name='owner',
            name='updated_at',
            field=models.DateTimeField(auto_now=True, null=True),
        ),
        migrations.AlterField(
            model_name='owner',
            name='phone',
            field=models.IntegerField(blank=True, max_length=11, null=True),
        ),
    ]
