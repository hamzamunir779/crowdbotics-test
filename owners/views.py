from rest_framework import viewsets
from rest_framework.decorators import detail_route
from rest_framework.response import Response

from owners.models import Owner
from owners.serializers import OwnerSerializer
from pets.models import Pet
from pets.serializers import PetSerializer


class OwnerViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """
    serializer_class = OwnerSerializer
    queryset = Owner.objects.all()

    @detail_route()
    def pet_list(self, request, pk=None):
        movie = self.get_object()  # retrieve an object by pk provided
        schedule = Pet.objects.filter(owner=pk).distinct()
        schedule_json = PetSerializer(schedule, many=True)
        return Response(schedule_json.data)