# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models

# Create your models here.

# -*- coding: utf-8 -*-

from django.contrib.auth import get_user_model
from django.db import models

# Create your models here.

User = get_user_model()

class Pet(models.Model):
    PETS = (
        ('D', 'Dog'),
        ('C', 'Cat'),
    )

    specie = models.CharField(max_length=1, choices=PETS)
    owner = models.ForeignKey(User, default=1)
    name = models.CharField(max_length=250)
    birthday = models.DateTimeField(null=True, blank=True)
    created_at = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    updated_at = models.DateTimeField(auto_now=True, null=True, blank=True)
