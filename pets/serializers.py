from rest_framework import serializers

from owners.models import Owner
from pets.models import Pet


class PetSerializer(serializers.ModelSerializer):
    class Meta:
        model = Pet
        fields = ('name', 'birthday', 'owner')