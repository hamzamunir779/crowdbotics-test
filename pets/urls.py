from rest_framework.routers import DefaultRouter

from owners.views import OwnerViewSet
from pets.views import PetViewSet

router = DefaultRouter()
router.register(r'pets', PetViewSet, base_name='pet')
urlpatterns = router.urls