from rest_framework import viewsets
from rest_framework.permissions import IsAuthenticated

from owners.models import Owner
from owners.serializers import OwnerSerializer
from pets.models import Pet
from pets.serializers import PetSerializer


class PetViewSet(viewsets.ModelViewSet):
    """
    A viewset for viewing and editing user instances.
    """

    serializer_class = PetSerializer
    queryset = Pet.objects.all()
    permission_classes = (IsAuthenticated,)

    def get_queryset(self):
        """
        Return a list of all pets.
        """
        # serializer = TransactionSerializer(data=request.data)
        # if serializer.is_valid():

        owner_pets = Pet.objects.filter(owner=self.request.user)

        return owner_pets
